import './Polyfill'

import Game from './Game.js'

const node = document.querySelector('#game')

const cv = document.createElement('canvas')
cv.width = node.clientWidth
cv.height = node.clientHeight

node.appendChild(cv)

let container = {
    canvas: cv
}

const game = new Game(container)