import Drawable from './Drawable'

class Playground extends Drawable {
    constructor(data, container){
        super(data, container)

        this.initial_parts = 4
        this.eat = false
    }

    draw(){
        this.head = this.parts[this.parts.length-1]

        if(this.eat){
            this.eat = false
        }
        else{
            this.parts.shift()
        }
        
        let tmp = {}
        
        switch(this.direction){
            case LEFT_ARROW:
                tmp.x = this.head.x-1
                tmp.y = this.head.y
                break
            case RIGHT_ARROW:
                tmp.x = this.head.x+1
                tmp.y = this.head.y
                break
            case UP_ARROW:
                tmp.x = this.head.x
                tmp.y = this.head.y-1
                break
            case DOWN_ARROW:
                tmp.x = this.head.x
                tmp.y = this.head.y+1
                break
        }

        this.parts.push(tmp)
        this.head = tmp        

        this.container.context.fillStyle = 'white'
        this.parts.forEach(function(part){
            this.container.context.fillRect(
                part.x*(this.container.grid.size+this.container.grid.offset), 
                part.y*(this.container.grid.size+this.container.grid.offset), 
                this.container.grid.size, 
                this.container.grid.size)
        }.bind(this))
    }

    collision(){
        if(this.head.x < 0){
            return true
        }
        if(this.head.y < 0){
            return true
        }
        if(this.head.x > this.container.grid.columns){
            return true
        }
        if(this.head.y > this.container.grid.lines){
            return true
        }
        for(let i = 0 ; i < this.parts.length-1 ; i++){
            if(this.head.x == this.parts[i].x && this.head.y == this.parts[i].y){
                return true
            }
        }

        return false
    }

    move(direction){
        if(this.direction == LEFT_ARROW && direction == RIGHT_ARROW){}
        else if(this.direction == RIGHT_ARROW && direction == LEFT_ARROW){}
        else if(this.direction == UP_ARROW && direction == DOWN_ARROW){}
        else if(this.direction == DOWN_ARROW && direction == UP_ARROW){}
        else{ this.direction = direction }
    }

    init(){
        this.direction = RIGHT_ARROW
        
        let center_x = Math.round(this.container.grid.columns/2)-this.initial_parts
        let center_y = Math.round(this.container.grid.lines/2)

        this.parts = []
        for(let i = 0 ; i < this.initial_parts ; i++){
            this.parts.push({ x: center_x, y: center_y })
            center_x++
        }

        this.head = this.parts[this.parts.length-1]
    }
}

export default Playground