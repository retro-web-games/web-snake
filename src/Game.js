import Core from './Core.js'

import Playground from './Objects/Playground'
import Snake from './Objects/Snake'
import Food from './Objects/Food'

class Game extends Core {
    constructor(container, opt){
        if(opt == null) opt = {}
        opt.max_fps = 20
        super(container, opt)        

        this.objects = {}
        this.objects.playground = new Playground({}, this.container)
        this.objects.snake = new Snake({}, this.container)

        document.addEventListener('keydown', function(event){
            this.objects.snake.move(event.keyCode)
        }.bind(this))

        this.death = 0

        this.init()
        this.run()
    }

    update(){
        if(this.objects.snake.collision()){
            this.init()
            this.death++
        }

        if(this.objects.snake.head.x == this.objects.food.x && this.objects.snake.head.y == this.objects.food.y){
            this.objects.snake.eat = true
            this.objects.food = new Food({}, this.container)
        }

        this.draw()

        this.container.context.fillText('SCORE ' + this.objects.snake.parts.length, 20, 60)
        this.container.context.fillText('DEATH ' + this.death, 20, 80)
    }

    init(){
        this.objects.food = new Food({}, this.container)
        this.objects.snake.init()
    }
}

export default Game